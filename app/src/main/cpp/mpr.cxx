//
// Created by ben09 on 2021/1/15.
//
//scp D:\WorkSpace\FreeLancer\Dicom\AndroidVTK\app\src\main\cpp\mpr.cxx root@120.25.24.191:~/vtk-android/ben/cpp
//scp root@120.25.24.191:~/vtk-android/ben/cpp/libAndroidMPR.so  D:\WorkSpace\FreeLancer\Dicom\AndroidVTK\app\src\main\jniLibs\armeabi-v7a\
//

#include <jni.h>
#include <errno.h>
#include <sstream>

#include "vtkNew.h"

#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkConeSource.h"
#include "vtkDebugLeaks.h"
#include "vtkGlyph3D.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkSphereSource.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"

#include "vtkImageData.h"
#include "vtkImageMapper.h"
#include "vtkImageResliceMapper.h"
#include "vtkImageSlice.h"
#include "vtkInteractorStyleImage.h"
#include "vtkNamedColors.h"
#include "vtkSmartPointer.h"
#include "vtkNamedColors.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"

#include "vtkAndroidRenderWindowInteractor.h"
#include "vtkCommand.h"

#include "vtkJPEGReader.h"
#include "vtkLookupTable.h"
#include "vtkImageReslice.h"
#include "vtkImageViewer2.h"
#include "vtkInteractorStyleImage.h"
#include "vtkTexture.h"
#include "vtkCylinderSource.h"

#include "vtkTransformTextureCoords.h"
#include "vtkTextureMapToCylinder.h"
#include "vtkPNGReader.h"
#include "vtkImageMapToColors.h"
#include "vtkDICOMImageReader.h"
#include "vtkMetaImageReader.h"
#include "vtkCellPicker.h"
#include "vtkImagePlaneWidget.h"
#include "vtkBMPReader.h"
#include "vtkTransform.h"
#include "vtkImageActor.h"
#include "vtkImageReader2.h"

#include <android/log.h>

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "AndroidVTK", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "AndroidVTK", __VA_ARGS__))

extern "C"
{
  JNIEXPORT jlong JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_init(
      JNIEnv *env,
      jobject obj,
      jint width,
      jint height,
      jstring imagePath);

  JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_render(
      JNIEnv *env,
      jobject obj,
      jlong renWinP);

  JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_onKeyEvent(
      JNIEnv *env,
      jobject obj,
      jlong udp,
      jboolean down,
      jint keyCode,
      jint metaState,
      jint repeatCount);

  JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_onMotionEvent(
      JNIEnv *env,
      jobject obj,
      jlong udp,
      jint action,
      jint eventPointer,
      jint numPtrs,
      jfloatArray xPos, jfloatArray yPos,
      jintArray ids, jint metaState);
};

struct userData
{
  vtkRenderWindow *RenderWindow;
  vtkRenderer *Renderer;
  vtkAndroidRenderWindowInteractor *Interactor;
};

// Example of updating text as we go
class vtkExampleCallback : public vtkCommand
{
public:
  static vtkExampleCallback *New()
  {
    return new vtkExampleCallback;
  }
  virtual void Execute(vtkObject *caller, unsigned long, void *)
  {
    // Update cardinality of selection
    double *pos = this->Camera->GetPosition();
    std::ostringstream txt;
    txt << "Camera positioned at: "
        << std::fixed
        << std::setprecision(2)
        << std::setw(6)
        << pos[0] << ", "
        << std::setw(6)
        << pos[1] << ", "
        << std::setw(6)
        << pos[2];
    this->Text->SetInput(txt.str().c_str());
  }

  vtkExampleCallback()
  {
    this->Camera = 0;
    this->Text = 0;
  }

  vtkCamera *Camera;
  vtkTextActor *Text;
};

JNIEXPORT jlong JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_init(JNIEnv *env, jobject obj, jint width, jint height, jstring imagePath)
{

  //imageViewer2
  // vtkNew<vtkImageViewer2> imageViewer;

  LOGI("ben : 圖像路徑位置 = %s", imagePath);
  const char *img = env->GetStringUTFChars(imagePath, 0); //string convert char (传入 string VTK 需要 char)
  LOGI("ben : 转换 图像位置为 char %s", img);

  LOGI("ben : JPG图取图片");
  vtkSmartPointer<vtkJPEGReader> imageReader = vtkSmartPointer<vtkJPEGReader>::New();
  imageReader->SetFileName(img); //图片读取 可以正常读取部份 jpg 图片 图片储存格式似乎会影响 reader 读取图片
  imageReader->Update();

  vtkSmartPointer<vtkJPEGReader> imageReader2 = vtkSmartPointer<vtkJPEGReader>::New();
  imageReader2->SetFileName("/storage/emulated/0/Download/11.jpg"); //图片读取 可以正常读取部份 jpg 图片 图片储存格式似乎会影响 reader 读取图片
  imageReader2->Update();

  LOGI("ben : 建立 LUB table");
  vtkSmartPointer<vtkLookupTable> lutTable = vtkSmartPointer<vtkLookupTable>::New(); //LUT table
  lutTable->SetRange(0.0, 255.0);
  lutTable->SetHueRange(0.1, 0.5);
  lutTable->SetValueRange(0.6, 0.1);
  lutTable->Build();

  vtkSmartPointer<vtkImageMapToColors> colorMap = vtkSmartPointer<vtkImageMapToColors>::New(); //颜色映射
  colorMap->SetInputData(imageReader2->GetOutput());                                           //https://blog.csdn.net/yuyangyg/article/details/78165570
  colorMap->SetLookupTable(lutTable);
  colorMap->Update();

  //目前还不能使用 PNGReader 读取 PNG 测试改用 imageReader2 搭配 imageView2 呈现画面 imageViewer2 取代 vtkRender
  // LOGI("ben : PNG图取图片");
  // vtkNew<vtkImageReader2> imageReader;
  // imageReader->SetFileName(img);
  // imageReader->Update();

  LOGI("ben : 建立图片 actor ");
  vtkSmartPointer<vtkImageActor> imageActor = vtkSmartPointer<vtkImageActor>::New(); //图片脚色
  imageActor->SetInputData(imageReader->GetOutput());

  LOGI("ben : 建立颜色 actor");
  vtkSmartPointer<vtkImageActor> colorActor = vtkSmartPointer<vtkImageActor>::New(); //颜色映射 actor
  colorActor->SetInputData(colorMap->GetOutput());

  // vtkSmartPointer<vtkTextActor> ta = vtkSmartPointer<vtkTextActor>::New(); //文字 可挡成 android textview
  // ta->SetInput("Droids Rock");
  // ta->GetTextProperty()->SetColor(1.0, 0.0, 0.0);
  // ta->SetDisplayPosition(50, 50);
  // ta->GetTextProperty()->SetFontSize(32);

  // vtkSmartPointer<vtkTextActor> ta2 = vtkSmartPointer<vtkTextActor>::New(); //文字 可挡成 android textview
  // ta2->SetInput("Droids Rock");
  // ta2->GetTextProperty()->SetColor(1.0, 0.0, 0.0);
  // ta2->SetDisplayPosition(50, 50);
  // ta2->GetTextProperty()->SetFontSize(32);

  LOGI("ben :建立 window 并与　render 绑定");
  vtkRenderWindow *window = vtkRenderWindow::New(); //建立 render
  char jniS[4] = {'j', 'n', 'i', 0};
  window->SetWindowInfo(jniS); // tell the system that jni owns the window not us
  window->SetSize(width, height);

  //配置 render 视窗位置  00 原點在左下角
  double imageViewport[4] = {0.0, 0.0, 1.0, 0.5};
  double colorViewport[4] = {0.0, 0.5, 1.0, 1.0};

  LOGI("ben : 建立图片 render 且与 actor 绑定");
  vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New(); //渲染 render
  window->AddRenderer(renderer.Get());                                         //添加 render 至 window
  renderer->SetViewport(colorViewport);                                        //配置颜色映射范围
  renderer->AddActor(imageActor.Get());                                        //配置腳色
  // renderer->AddActor(ta.Get());                                                //配置 文字腳色
  renderer->ResetCamera();                //重製敬頭
  renderer->SetBackground(0.4, 0.5, 0.6); //配置 被景色

  vtkSmartPointer<vtkRenderer> colorRender = vtkSmartPointer<vtkRenderer>::New(); //建立颜色映射 render
  window->AddRenderer(colorRender.Get());
  colorRender->SetViewport(imageViewport);
  colorRender->AddActor(colorActor.Get());
  // colorRender->AddActor(ta2.Get());
  colorRender->ResetCamera();
  colorRender->SetBackground(1.0, 1.0, 0.8);

  LOGI("ben : 设置 android 操作者");
  vtkSmartPointer<vtkAndroidRenderWindowInteractor> iren = vtkSmartPointer<vtkAndroidRenderWindowInteractor>::New(); //配置 操作者
  vtkSmartPointer<vtkInteractorStyleImage> style = vtkSmartPointer<vtkInteractorStyleImage>::New();                  //手指滑动图像 配置后不会滑动图片 有条窗效果

  iren->SetInteractorStyle(style); //配置 style
  iren->SetRenderWindow(window);   //配置視窗

  //設置 互動事件
  // vtkSmartPointer<vtkExampleCallback> cb = vtkSmartPointer<vtkExampleCallback>::New();
  // cb->Camera = renderer->GetActiveCamera();
  // cb->Text = ta.Get();
  // iren->AddObserver(vtkCommand::InteractionEvent, cb.Get());

  // vtkSmartPointer<vtkExampleCallback> cb2 = vtkSmartPointer<vtkExampleCallback>::New();
  // cb2->Camera = colorRender->GetActiveCamera();
  // cb2->Text = ta2.Get();
  // iren->AddObserver(vtkCommand::InteractionEvent, cb2.Get());

  struct userData *foo = new struct userData(); //渲染結果
  foo->RenderWindow = window;
  foo->Renderer = renderer.Get();
  foo->Interactor = iren.Get();
  return (jlong)foo;
}

JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_render(JNIEnv *env, jobject obj, jlong udp)
{
  struct userData *foo = (userData *)(udp);
  foo->RenderWindow->SwapBuffersOff(); // android does it
  foo->Interactor->Initialize();
  foo->RenderWindow->Render();
  foo->RenderWindow->SwapBuffersOn(); // reset
}

JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_onKeyEvent(JNIEnv *env, jobject obj, jlong udp,
                                                                       jboolean down, jint keyCode, jint metaState, jint repeatCount)
{
  struct userData *foo = (userData *)(udp);
  foo->Interactor->HandleKeyEvent(down, keyCode, metaState, repeatCount);
}

JNIEXPORT void JNICALL Java_com_clear_vtk_mpr_AndroidMPRLib_onMotionEvent(JNIEnv *env, jobject obj, jlong udp,
                                                                          jint action,
                                                                          jint eventPointer,
                                                                          jint numPtrs,
                                                                          jfloatArray xPos, jfloatArray yPos,
                                                                          jintArray ids, jint metaState)
{
  struct userData *foo = (userData *)(udp);

  int xPtr[VTKI_MAX_POINTERS];
  int yPtr[VTKI_MAX_POINTERS];
  int idPtr[VTKI_MAX_POINTERS];

  // only allow VTKI_MAX_POINTERS touches right now
  if (numPtrs > VTKI_MAX_POINTERS)
  {
    numPtrs = VTKI_MAX_POINTERS;
  }

  // fill in the arrays
  jfloat *xJPtr = env->GetFloatArrayElements(xPos, 0);
  jfloat *yJPtr = env->GetFloatArrayElements(yPos, 0);
  jint *idJPtr = env->GetIntArrayElements(ids, 0);
  for (int i = 0; i < numPtrs; ++i)
  {
    xPtr[i] = (int)xJPtr[i];
    yPtr[i] = (int)yJPtr[i];
    idPtr[i] = idJPtr[i];
  }
  env->ReleaseIntArrayElements(ids, idJPtr, 0);
  env->ReleaseFloatArrayElements(xPos, xJPtr, 0);
  env->ReleaseFloatArrayElements(yPos, yJPtr, 0);

  foo->Interactor->HandleMotionEvent(action, eventPointer, numPtrs, xPtr, yPtr, idPtr, metaState);
}
