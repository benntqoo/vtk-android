cmake_minimum_required(VERSION 3.4.1)
set(CMAKE_SYSTEM_NAME Android)
set(CMAKE_SYSTEM_VERSION "24")
set(CMAKE_ANDROID_ARCH_ABI "armeabi-v7a")

#### 修改你 ndk 目錄位置
#set(CMAKE_ANDROID_NDK $ENV{NDK_HOME})
set(CMAKE_ANDROID_NDK "/root/vtk-android/ndk/android-ndk-r16b")

#### vtk 靜態庫配置
set(VTK_DIR ${CMAKE_CURRENT_SOURCE_DIR}/vtk-android/lib/cmake/vtk-8.2)

project(AndroidMPR)


find_package(VTK REQUIRED)

include(${VTK_USE_FILE})

set(sources  mpr.cxx)

add_library(AndroidMPR SHARED ${sources})
target_link_libraries(AndroidMPR android  log  EGL GLESv3 stdc++ ${VTK_LIBRARIES})
