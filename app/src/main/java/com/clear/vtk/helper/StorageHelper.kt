package com.clear.vtk.helper

import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.Parcelize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import kotlin.coroutines.CoroutineContext

class StorageHelper : CoroutineScope {
    companion object {
        private const val TAG = "StorageHelper"
    }

    var job: Job? = null
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    val storageList: MutableList<StorageInfo> = mutableListOf()
    suspend fun getAllExternalStoreCount(): Int = withContext(Dispatchers.IO) {
        storageList.clear()
        RunTimeHelper.getShellResult("mount").forEach mount@{ lineStr ->
//            Log.e(TAG, "getAllExternalStoreCount: $lineStr")
            val position = ordinalIndexOf(lineStr, RunTimeHelper.COMMAND_SPLIT, 5)//獲取必須資訊長度
            val line = lineStr.substring(0, position)
//            Log.i(TAG, "getAllExternalStoreCount: 分区过滤前 lineStr = $line")
            // 将常见的linux分区过滤掉
            if (line.contains("proc") || line.contains("tmpfs") ||
                line.contains("asec") || line.contains("secure") || line.contains("system") ||
                line.contains("cache") || line.contains("sys") || line.contains("data") ||
                line.contains("shell") || line.contains("root") || line.contains("acct") ||
                line.contains("misc") || line.contains("obb") ||
                line.contains("write") || line.contains("default") || line.contains("read")
            ) return@mount
//            Log.i(TAG, "getAllExternalStoreCount: 分区过滤后 lineStr = $line")

            //這裡是需要得分區 需要額外U盤格是在這裡添加判斷
            if (line.contains("fat") || line.contains("fuse") || line.contains("ntfs") ||
                line.contains("ext3") || line.contains("ext4") || line.contains("ext2") ||
                line.contains("sdcardfs")
            ) {
                val items = line.split(RunTimeHelper.COMMAND_SPLIT)
//                Log.i(TAG, "getAllExternalStoreCount: items = $items")
                if (items.size > 1) {
                    val info = StorageInfo()
                    val path = items[2]
                    info.path = path + File.separator

                    val fileSystem = when (items[4]) {
                        StorageType.EXFAT.value -> StorageType.EXFAT
                        StorageType.FAT16.value -> StorageType.EXFAT
                        StorageType.FAT32.value -> StorageType.FAT32
                        StorageType.FUSE.value -> StorageType.FUSE
                        StorageType.NTFS.value -> StorageType.NTFS
                        StorageType.EXT2.value -> StorageType.EXT2
                        StorageType.EXT3.value -> StorageType.EXT3
                        StorageType.EXT4.value -> StorageType.EXT4
                        else -> null
                    }
                    info.fileSystem = fileSystem?.value ?: ""
                    //硬碟空間暫不計算

                    if (!path.isBlank() && path.contains("sd") || path.contains("mmcblk") || path.contains("storage")) {
                        val index = storageList.indexOf(info)
//                        Log.i(TAG, "getAllExternalStoreCount: 添加外部装置 index = $index")
                        if (index >= 0) {
                            storageList.removeAt(index)
                            storageList.add(index, info)
                        } else storageList.add(info)
                    }
                }
            }
        }
        return@withContext storageList.size
    }

    /**
     *
     * Copy from Apache Commons StringUtils
     * @param str 源字串
     * @param subStr 標記字串
     * @param n 重複次數
     * @return pos 標記字串重複次數後的位置
     */
    private fun ordinalIndexOf(str: String, subStr: String, n: Int): Int {
        var pos = str.indexOf(subStr)
        var count = n
        while (--count > 0 && pos != -1) pos = str.indexOf(subStr, pos + 1)
        return pos
    }

    fun stopJob() {
        job?.cancel()
        job = null
    }
}


enum class StorageType(var value: String) {
    EXFAT("exFAT"),
    FAT16("FAT16"),
    FAT32("FAT32"),
    FUSE("FUSE"),
    NTFS("NTFS"),
    EXT2("EXT2"),
    EXT3("EXT3"),
    EXT4("EXT4")
}

/**
 * 外部装置资料
 */
@Parcelize
data class StorageInfo(
    var path: String = "",
    var fileSystem: String = "",
    var totalSpace: String = "",
    var usedSpace: String = "",
    var freeSpace: String = ""
) : Parcelable {
    override fun equals(other: Any?): Boolean {
        return if (other is StorageInfo) path == other.path
        else super.equals(other)
    }

    override fun hashCode(): Int {
        var result = path.hashCode()
        result = 31 * result + fileSystem.hashCode()
        result = 31 * result + totalSpace.hashCode()
        result = 31 * result + usedSpace.hashCode()
        result = 31 * result + freeSpace.hashCode()
        return result
    }
}