package com.clear.vtk

import android.app.Application
import com.clear.vtk.di.koinModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class VTKApplication : Application() {
    companion object {
        private const val TAG = "VTKApplication"
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@VTKApplication)
            modules(koinModule)
        }
    }
}