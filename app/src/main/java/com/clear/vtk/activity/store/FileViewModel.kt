package com.clear.vtk.activity.store

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.clear.vtk.helper.StorageHelper
import com.clear.vtk.repository.FileRepository
import com.snatik.storage.helpers.OrderType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.*

class FileViewModel(private val fileRepository: FileRepository) : ViewModel() {
    companion object {
        private const val TAG = "FileViewModel"

        private const val DingDing = "/storage/emulated/0/DingTalk"
    }

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()


    private val storageHelper = StorageHelper()


    val files: MutableLiveData<List<File>> = MutableLiveData()


    fun setDirTree(value: Int) {
        fileRepository.dirTree = value
        Log.e(TAG, "setDirTree: tree = ${fileRepository.dirTree}")
    }

    fun getDirTree() = fileRepository.dirTree
    fun getRootPath() = fileRepository.rootPath
    fun getCurrentPath() = fileRepository.currentPath.value
    fun switchRoot(isSwitch: Boolean) {
        fileRepository.rootPath = if (isSwitch) "" else fileRepository.defaultRootPath
        fileRepository.dirTree = 0
        showFile(fileRepository.rootPath)
    }

    fun showFile(path: String) {
        isLoading.value = true
        Log.i(TAG, "showFile: 开启路径 $path")
        fileRepository.currentPath.value = path

        GlobalScope.launch(Dispatchers.Main) {
            if (path.isBlank()) {
                val storageSize = storageHelper.getAllExternalStoreCount()
                Log.i(TAG, "showFile: 使用U盘与丁丁目录 storage size = $storageSize")

                val list: MutableList<File> = mutableListOf()
                val dingDing = File(DingDing)
                Log.i(TAG, "showFile: dingDing exists = ${dingDing.exists()}")
                if (dingDing.exists()) list.add(dingDing)
                storageHelper.storageList.forEach {
                    val storageFile = File(it.path)
                    Log.i(TAG, "showFile: path = ${it.path} U盘存不存在 ${storageFile.exists()}")
                    if (storageFile.exists()) list.add(storageFile)
                }

                if (list.size == 0) {
                    Log.i(TAG, "showFile: 无挡案 开启预设路径")
                    fileRepository.rootPath = fileRepository.defaultRootPath
                    showFile(fileRepository.defaultRootPath)
                    return@launch
                }

                files.value = list
                isLoading.value=false
            } else {
                Log.i(TAG, "showFile: 使用预设目录")
                val list = getFiles(path, true, ".jpg", ".jpeg", ".png")
                if (list.isNotEmpty()) Collections.sort(list, OrderType.NAME.comparator)

                files.value = list
                isLoading.value=false
            }
        }
    }

    fun getFiles(
        dir: String,
        hasDir: Boolean = true,
        vararg matchRegex: String = arrayOf("")
    ): MutableList<File> {
        val file = File(dir)
        val result: Array<File>? = if (matchRegex.isNotEmpty()) {
            file.listFiles { root, name ->
                when {
                    name.startsWith(".") -> false//隐藏文件
                    hasDir && File(root, name).isDirectory -> true//添加目录
                    else -> {
                        var isSuccess = false
                        matchRegex.forEach {
                            if (name.endsWith(it)) {
                                isSuccess = true
                                return@forEach
                            }
                        }
                        isSuccess
                    }
                }
            }
        } else file.listFiles()

        return result?.toMutableList() ?: mutableListOf()
    }
}