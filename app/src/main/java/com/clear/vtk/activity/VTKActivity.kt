package com.clear.vtk.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.clear.vtk.mpr.MPRView

class VTKActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "VTKActivity"

        private const val KEY = "image"

        fun openImage(activity: AppCompatActivity, path: String) {
            activity.startActivity(Intent(activity, VTKActivity::class.java).apply {
                putExtras(Bundle().apply {
                    putString(KEY, path)
                })
            })
        }
    }

    private lateinit var vtkView: MPRView
    private var image: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        image = intent.extras?.getString(KEY, "") ?: ""

        vtkView = MPRView(applicationContext)
        vtkView.imagePath = image
        if (image.isEmpty()) {
            finish()
            return
        } else setContentView(vtkView)
    }

    override fun onPause() {
        super.onPause()
        this.vtkView.onPause()
    }

    override fun onResume() {
        super.onResume()
        this.vtkView.onResume()
    }
}