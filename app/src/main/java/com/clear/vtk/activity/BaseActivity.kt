package com.clear.vtk.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.clear.vtk.helper.ScreenHelper

abstract class BaseActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "BaseActivity"
    }

    abstract fun hasDataBinding(@LayoutRes layoutRes: Int): Boolean

    /**
     * 插入的 layout xml
     */
    @LayoutRes
    abstract fun layoutRes(): Int

    /**
     * onCreate 處裡一些初始設定後 呼叫此方法執行 view 初始化
     * 等同於 onCreate
     *
     */
    abstract fun onViewSetting()

    /**
     * 保存數據
     */
    abstract fun onSaveInstance(state: Bundle)

    /**
     * 恢復數據 或 intent 來的數據 bundle
     */
    abstract fun onRestoreInstance(state: Bundle)

    var versionAlert: AlertDialog? = null
    override fun onSaveInstanceState(outState: Bundle) {
        onSaveInstance(outState)
        super.onSaveInstanceState(outState)
    }

    abstract fun bindData()
    abstract fun unbindData()
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        onRestoreInstance(savedInstanceState)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ScreenHelper.setFullScreen(this, true)
        if (!hasDataBinding(layoutRes())) setContentView(layoutRes())
        bindData()
        onViewSetting()
    }

    override fun onDestroy() {
        versionAlert?.dismiss()
        versionAlert = null
        unbindData()
        super.onDestroy()
    }

    fun showInstallApk(ok: () -> Unit, cancel: () -> Unit) {
        versionAlert = AlertDialog.Builder(this)
            .setTitle("New Version")
            .setCancelable(false)
            .setMessage("发现新版本是否更新?")
            .setPositiveButton("OK") { _, _ ->
                ok()
            }
            .setNegativeButton("Cancel") { _, _ ->
                cancel()
            }.show()
    }
}