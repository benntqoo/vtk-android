package com.clear.vtk.activity.store

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.clear.vtk.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.clear.vtk.activity.BaseActivity
import com.clear.vtk.activity.VTKActivity
import com.clear.vtk.databinding.ActivityFileBinding
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class FileActivity : BaseActivity(), FilesAdapter.OnFileItemListener {
    companion object {
        private const val TAG = "StorageActivity"
    }

    private val PERMISSION_REQUEST_CODE = 1000

    private lateinit var bind: ActivityFileBinding
    private val vm: FileViewModel by viewModel()
    private val adapter: FilesAdapter by lazy { FilesAdapter(this) }

    override fun hasDataBinding(layoutRes: Int): Boolean {
        bind = DataBindingUtil.setContentView(this, R.layout.activity_file)
        bind.lifecycleOwner = this
        bind.vm = vm
        return true
    }

    override fun layoutRes(): Int = R.layout.activity_file

    override fun onViewSetting() {
        Log.e(TAG, "onViewSetting: ${vm.getDirTree()}")
        bind.ivBack.setOnClickListener {
            Log.i(TAG, "onViewSetting: ${vm.getDirTree()}")
            if (vm.getDirTree() <= 0) return@setOnClickListener
            onBackPressed()
        }

        bind.srl.setOnRefreshListener { vm.showFile(vm.getCurrentPath() ?: vm.getRootPath()) }
        bind.rvFile.layoutManager = LinearLayoutManager(this)
        bind.rvFile.adapter = adapter
        adapter.setListener(this)


        bind.switchPath.isChecked = true
        bind.switchPath.setOnCheckedChangeListener { _, isChecked ->
            vm.switchRoot(isChecked)
        }

    }

    override fun onSaveInstance(state: Bundle) {
    }

    override fun onRestoreInstance(state: Bundle) {

    }

    override fun bindData() {
        vm.files.observe(this, Observer { adapter.setFiles(it) })
    }

    override fun unbindData() {
        vm.files.removeObservers(this)
    }

    override fun onStart() {
        super.onStart()
        checkPermission()
    }

    override fun onFileClick(file: File?) {
        file?.let {
            if (it.isDirectory) {
                vm.setDirTree(vm.getDirTree() + 1)
                vm.showFile(it.absolutePath)
            } else VTKActivity.openImage(this, file.absolutePath)
        }
    }

    override fun onLongClick(file: File?) {

    }

    override fun onBackPressed() {
        if (vm.getDirTree() > 0) {
            val path = getPreviousPath()
            if (vm.getDirTree() == 0) vm.showFile(vm.getCurrentPath() ?: vm.getRootPath())
            else vm.showFile(path)
            vm.setDirTree(vm.getDirTree() - 1)
        } else super.onBackPressed()
    }

    private fun getCurrentPath(): String = vm.getCurrentPath() ?: vm.getRootPath()

    private fun getPreviousPath(): String = vm.getCurrentPath()?.let { path ->
        val lastIndexOf = path.lastIndexOf(File.separator)
        if (lastIndexOf <= 0) getCurrentPath()
        else path.substring(0, lastIndexOf)
    } ?: vm.getRootPath()


    private fun checkPermission() {
        Log.i(TAG, "checkPermission: ")
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED -> ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED -> ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
            else -> vm.showFile(vm.getCurrentPath() ?: vm.getRootPath())
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) checkPermission()
            else missPermission()
        } else missPermission()
    }

    private fun missPermission() {
        Log.i(
            TAG,
            "missPermission: ${ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )}"
        )
        if (!ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            MaterialAlertDialogBuilder(this)
                .setTitle("提示")
                .setMessage("请开启权限")
                .setNegativeButton("离开") { _, _ -> finish() }
                .setPositiveButton("手动开启权限") { _, _ -> startToPermissionSettingIntent(this) }
                .show()

        } else if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(
                this,
                "Need to External Storage Permission,Otherwise cannot be used normally.",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun startToPermissionSettingIntent(context: Context) {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", context.packageName, null)
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}