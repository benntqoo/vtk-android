package com.clear.vtk.base

import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLDisplay

class ConfigChooser : GLSurfaceView.EGLConfigChooser {
    companion object {
        private const val TAG = "ConfigChooser"

        /* This EGL config specification is used to specify 2.0 rendering.
         * We use a minimum size of 4 bits for red/green/blue, but will
         * perform actual matching in chooseConfig() below.
         */
        private const val EGL_OPENGL_ES2_BIT = 4
        private val s_configAttribs2 = intArrayOf(
            EGL10.EGL_RED_SIZE, 4,
            EGL10.EGL_GREEN_SIZE, 4,
            EGL10.EGL_BLUE_SIZE, 4,
            EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,/* EGL_OPENGL_ES2_BIT */
            EGL10.EGL_NONE
        )
    }

    // Subclasses can adjust these values:
    private var redSize = 0
    private var greenSize = 0
    private var blueSize = 0
    private var alphaSize = 0
    private var depthSize = 0
    private var stencilSize = 0
    private val value = IntArray(1)

    constructor(r: Int, g: Int, b: Int, a: Int, depth: Int, stencil: Int) {
        redSize = r
        greenSize = g
        blueSize = b
        alphaSize = a
        depthSize = depth
        stencilSize = stencil
    }

    override fun chooseConfig(egl: EGL10, display: EGLDisplay): EGLConfig? {
        // Get the number of minimally matching EGL configurations
        val numConfig = IntArray(1)
        egl.eglChooseConfig(display, s_configAttribs2, null, 0, numConfig)

        val numConfigs = numConfig[0]

        require(numConfigs > 0) { "No configs match configSpec" }


        // Allocate then read the array of minimally matching EGL configs
        val configs = arrayOfNulls<EGLConfig>(numConfigs)
        egl.eglChooseConfig(display, s_configAttribs2, configs, numConfigs, numConfig)

        // Now return the "best" one
        return chooseConfig(egl, display, configs)
    }

    fun chooseConfig(egl: EGL10, display: EGLDisplay, configs: Array<EGLConfig?>): EGLConfig? {
        for (config in configs) {
            val d: Int = findConfigAttrib(egl, display, config, EGL10.EGL_DEPTH_SIZE)
            val s: Int = findConfigAttrib(egl, display, config, EGL10.EGL_STENCIL_SIZE)

            // We need at least mDepthSize and mStencilSize bits
            if (d < depthSize || s < stencilSize) continue

            // We want an *exact* match for red/green/blue/alpha
            val r: Int = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE)
            val g: Int = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE)
            val b: Int = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE)
            val a: Int = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE)
            if (r == redSize && g == greenSize && b == blueSize && a == alphaSize) return config
        }
        return null
    }

    private fun findConfigAttrib(
        egl: EGL10,
        display: EGLDisplay,
        config: EGLConfig?,
        attribute: Int,
        defaultValue: Int = 0
    ): Int {
        return if (egl.eglGetConfigAttrib(
                display,
                config,
                attribute,
                value
            )
        ) value[0] else defaultValue
    }
}