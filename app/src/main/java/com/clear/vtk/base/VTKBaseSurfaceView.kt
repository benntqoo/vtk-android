package com.clear.vtk.base

import android.content.Context
import android.graphics.PixelFormat
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

abstract class VTKBaseSurfaceView : GLSurfaceView, VTKControl {
    companion object {
        private const val TAG = "VTKBaseSurfaceView"
    }

    lateinit var render: VTKRender

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    fun initRender(control: VTKControl) {
        isFocusable = true
        isFocusableInTouchMode = true
        holder.setFormat(PixelFormat.TRANSLUCENT)

        setEGLContextClientVersion(2)
        setEGLContextFactory(ContextFactory())
        setEGLConfigChooser(ConfigChooser(8, 8, 8, 0, 8, 0))

        render = VTKRender(control)
        setRenderer(render)
        renderMode = RENDERMODE_WHEN_DIRTY
    }

    class VTKRender(private val control: VTKControl) : Renderer {
        private var vtkContext: Long = 0L
        override fun onDrawFrame(gl: GL10) {
            control.onDrawFrame(gl, vtkContext)
        }

        override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
            vtkContext = control.onSurfaceChange(gl, width, height)
        }

        override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {

        }

        // forward events to VTK for it to handle
        fun onKeyEvent(down: Boolean, keyEvent: KeyEvent) {
            control.onKeyEvent(vtkContext, down, keyEvent)
        }

        // forward events to VTK for it to handle
        fun onMotionEvent(motionEvent: MotionEvent) {
            control.onMotionEvent(vtkContext, motionEvent)
        }
    }

    // forward events to rendering thread for it to handle
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        queueEvent { render.onKeyEvent(true, event) }
        return true
    }

    // forward events to rendering thread for it to handle
    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        queueEvent { render.onKeyEvent(false, event) }
        return true
    }

    // forward events to rendering thread for it to handle
    override fun onTouchEvent(event: MotionEvent): Boolean {
        queueEvent { render.onMotionEvent(event) }
        return true
    }
}