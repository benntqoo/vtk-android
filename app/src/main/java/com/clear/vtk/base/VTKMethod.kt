package com.clear.vtk.base

import android.util.Log
import javax.microedition.khronos.egl.EGL10

fun checkEglError(tag: String,prompt:String, egl: EGL10?) {
    egl ?: run {
        Log.e("checkEglError", "$tag: egl is null.")
        return
    }

    var error: Int
    while (egl.eglGetError().also { error = it } != EGL10.EGL_SUCCESS) {
        Log.e(tag, String.format("%s: EGL error: 0x%x", prompt, error))
    }
}
