package com.clear.vtk.base

import android.opengl.GLSurfaceView
import android.util.Log
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.egl.EGLDisplay

class ContextFactory : GLSurfaceView.EGLContextFactory {
    companion object {
        private const val TAG = "ContextFactory"
        private const val EGL_CONTEXT_CLIENT_VERSION: Int = 0x3098
    }

    override fun createContext(
        egl: EGL10?,
        display: EGLDisplay?,
        eglConfig: EGLConfig?
    ): EGLContext {
        Log.e(TAG, "creating OpenGL ES 2.0 context")
        checkEglError(TAG, "Before eglCreateContext", egl)
        val attrList = intArrayOf(EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE)
        val context = egl!!.eglCreateContext(display, eglConfig, EGL10.EGL_NO_CONTEXT, attrList)
        checkEglError(TAG, "After eglCreateContext", egl)
        return context
    }

    override fun destroyContext(egl: EGL10?, display: EGLDisplay?, context: EGLContext?) {
        egl?.eglDestroyContext(display, context)
    }
}