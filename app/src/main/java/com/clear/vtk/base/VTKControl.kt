package com.clear.vtk.base

import android.view.KeyEvent
import android.view.MotionEvent
import javax.microedition.khronos.opengles.GL10

interface VTKControl {
    fun onKeyEvent(vtkContext: Long, down: Boolean, keyEvent: KeyEvent)
    fun onMotionEvent(vtkContext: Long, motionEvent: MotionEvent)

    /**
     * 回传 vtk context long
     */
    fun onSurfaceChange(gl: GL10, width: Int, height: Int): Long
    fun onDrawFrame(gl: GL10, vtkContext: Long)
}