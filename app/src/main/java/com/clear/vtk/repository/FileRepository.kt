package com.clear.vtk.repository

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.snatik.storage.Storage

class FileRepository(context: Context) {
    companion object {
        private const val TAG = "FileRepository"
    }

    var dirTree: Int = 0
    private val storage: Storage = Storage(context)
    val defaultRootPath: String = storage.externalStorageDirectory
        get() {
            return field
        }

    var rootPath: String = ""

    val currentPath: MutableLiveData<String> = MutableLiveData()


}