package com.clear.vtk.mpr

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import com.clear.vtk.base.VTKBaseSurfaceView
import com.clear.vtk.base.VTKControl
import java.io.File
import javax.microedition.khronos.opengles.GL10

class MPRView : VTKBaseSurfaceView {
    companion object {
        private const val TAG = "MPRView"
    }

    var imagePath: String = ""

    constructor(context: Context?) : this(context,null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initRender(this)
    }

    override fun onKeyEvent(vtkContext: Long, down: Boolean, keyEvent: KeyEvent) {
        AndroidMPRLib.onKeyEvent(
            vtkContext,
            down,
            keyEvent.keyCode,
            keyEvent.metaState,
            keyEvent.repeatCount
        )
    }

    override fun onMotionEvent(vtkContext: Long, motionEvent: MotionEvent) {
        try {
            val numPtrs: Int = motionEvent.pointerCount
            val xPos = FloatArray(numPtrs)
            val yPos = FloatArray(numPtrs)
            val ids = IntArray(numPtrs)
            for (i in 0 until numPtrs) {
                ids[i] = motionEvent.getPointerId(i)
                xPos[i] = motionEvent.getX(i)
                yPos[i] = motionEvent.getY(i)
            }
            val actionIndex: Int = motionEvent.actionIndex
            val actionMasked: Int = motionEvent.actionMasked
            val actionId: Int = motionEvent.getPointerId(actionIndex)
            if (actionMasked != 2) {
                Log.e(TAG, "Got action $actionMasked on index $actionIndex has id $actionId")
            }
            AndroidMPRLib.onMotionEvent(
                vtkContext,
                actionMasked,
                actionId,
                numPtrs, xPos, yPos, ids,
                motionEvent.metaState
            )
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "Bogus motion event")
        }
    }

    override fun onSurfaceChange(gl: GL10, width: Int, height: Int): Long {
        val image = File(imagePath)
        Log.e(TAG, "onSurfaceChange: 圖片是否存在 = ${image.exists()} path = $imagePath")
        return AndroidMPRLib.init(width, height, imagePath)
    }

    override fun onDrawFrame(gl: GL10, vtkContext: Long) {
        AndroidMPRLib.render(vtkContext)
    }
}