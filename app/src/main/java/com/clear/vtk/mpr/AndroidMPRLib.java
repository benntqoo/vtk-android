package com.clear.vtk.mpr;

class AndroidMPRLib {
    static {
        System.loadLibrary("AndroidMPR");
    }

    public static native long init(int width, int height,String imagePath);

    public static native void render(long udp);

    public static native void onKeyEvent(long udp, boolean down, int keyCode,
                                         int metaState,
                                         int repeatCount);

    public static native void onMotionEvent(long udp,
                                            int action,
                                            int eventPointer,
                                            int numPtrs,
                                            float[] xPos, float[] yPos, int[] ids,
                                            int metaState);
}
