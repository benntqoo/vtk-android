package com.clear.vtk.di

import com.clear.vtk.activity.store.FileViewModel
import com.clear.vtk.repository.FileRepository
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val koinModule = module {
    single { FileRepository(get()) }
    viewModel { FileViewModel(get()) }
}