package com.clear.vtk.framework

import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

object BindAdapter {
    private const val TAG = "BindAdapter"

    @JvmStatic
    @BindingAdapter("bindSwipeRefreshLayout")
    fun isSwipeRefresh(swipeRefreshLayout: SwipeRefreshLayout, isShow: Boolean) {
        swipeRefreshLayout.isRefreshing = isShow
    }
}